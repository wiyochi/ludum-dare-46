package views;

import flixel.FlxG;
import flixel.addons.editors.ogmo.FlxOgmo3Loader;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.FlxState;

class GameState extends FlxState
{
	var actualSide:Sides = CASTLE;
	var lands:Array<Land> = new Array();
	var castle:Castle;
	

	override public function create()
	{

		if (FlxG.sound.music == null) // don't restart the music if it's already playing
		{
			FlxG.sound.playMusic(AssetPaths.black_lamp___intro__ogg, 1, true);
			FlxG.sound.volume = 0.1;
		}
		
		super.create();

		lands[0] = new Land("East");
		lands[1] = new Land("West");
		lands[2] = new Land("North");
		lands[3] = new Land("South");
		castle = new Castle();
		
		add(castle);
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);
	}

	public function changeState(side:Sides)
	{
		clear();
		switch (side)
		{
			case EAST:
				add(lands[0]);
			case WEST:
				add(lands[1]);
			case NORTH:
				add(lands[2]);
			case SOUTH:
				add(lands[3]);
			case CASTLE:
				add(castle);
		}
	}
}
