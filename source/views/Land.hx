package views;

import entities.Enemy;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.addons.editors.ogmo.FlxOgmo3Loader;
import flixel.group.FlxGroup;
import flixel.math.FlxPoint;
import flixel.text.FlxText.FlxTextAlign;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import haxe.display.Display.CompletionMode;
import js.html.Console;

class TowerSpawner
{
	var position:FlxPoint;

	public var empty:Bool;

	public function new(x:Float, y:Float)
	{
		position = new FlxPoint(x, y);
		empty = true;
	}

	public function getPosition():FlxPoint
	{
		return position;
	}
}

class Land extends FlxGroup
{
	var landName:String = "Mother";
	var playButton:FlxButton;
	var map:FlxOgmo3Loader;
	var walls:FlxTilemap;
	var mapSpawner:FlxTilemap;

	var spawners:Array<FlxPoint>;
	var nbSpawners:Int;
	var towerSpawners:Array<TowerSpawner>;

	var path_1:Array<FlxPoint>;
	var path_2:Array<FlxPoint>;
	var path_3:Array<FlxPoint>;

	var towers:Array<Tower>;
	var enemies:Array<Enemy>;
	var shots:Array<Shot>;

	var wave:Wave;

	public function new(name:String)
	{
		super();

		towers = new Array<Tower>();
		enemies = new Array<Enemy>();
		shots = new Array<Shot>();

		this.landName = name;
		var text = new flixel.text.FlxText(10, 10, 0, landName, 24);
		playButton = new FlxButton(10, 50, "Castle", function()
		{
			Main.game.changeState(Sides.CASTLE);
		});
		playButton.label.setFormat(20, FlxColor.BLACK, FlxTextAlign.CENTER);
		playButton.scale.set(2, 2);
		playButton.updateHitbox();
		if (name == "East")
		{
			map = new FlxOgmo3Loader(AssetPaths.ludum_dare_04_2020__ogmo, AssetPaths.east__json);
			path_1 = [
				new FlxPoint(805, 90),
				new FlxPoint(670, 90),
				new FlxPoint(670, 50),
				new FlxPoint(590, 50),
				new FlxPoint(590, 10),
				new FlxPoint(510, 10),
				new FlxPoint(510, 90),
				new FlxPoint(180, 90)
			];
			path_2 = [
				new FlxPoint(805, 290),
				new FlxPoint(710, 290),
				new FlxPoint(710, 210),
				new FlxPoint(550, 210),
				new FlxPoint(550, 290),
				new FlxPoint(180, 290)
			];
			path_3 = [
				new FlxPoint(805, 530),
				new FlxPoint(710, 530),
				new FlxPoint(710, 450),
				new FlxPoint(630, 450),
				new FlxPoint(630, 525),
				new FlxPoint(510, 525),
				new FlxPoint(510, 490),
				new FlxPoint(180, 480)
			];
			nbSpawners = 3;
		}
		else if (name == "West")
		{
			map = new FlxOgmo3Loader(AssetPaths.ludum_dare_04_2020__ogmo, AssetPaths.west__json);
			path_1 = [
				new FlxPoint(180, 90),
				new FlxPoint(310, 90),
				new FlxPoint(310, 50),
				new FlxPoint(390, 50),
				new FlxPoint(390, 10),
				new FlxPoint(470, 10),
				new FlxPoint(470, 90),
				new FlxPoint(805, 90)
			];
			path_2 = [
				new FlxPoint(180, 290),
				new FlxPoint(270, 290),
				new FlxPoint(270, 370),
				new FlxPoint(430, 370),
				new FlxPoint(430, 290),
				new FlxPoint(805, 290)
			];
			path_3 = [
				new FlxPoint(180, 530),
				new FlxPoint(270, 530),
				new FlxPoint(270, 450),
				new FlxPoint(350, 450),
				new FlxPoint(350, 530),
				new FlxPoint(470, 530),
				new FlxPoint(470, 490),
				new FlxPoint(805, 490)
			];
			nbSpawners = 3;
		}
		else if (name == "North")
		{
			map = new FlxOgmo3Loader(AssetPaths.ludum_dare_04_2020__ogmo, AssetPaths.north__json);
			path_1 = [
				new FlxPoint(310, 5), new FlxPoint(310, 50), new FlxPoint(350, 50), new FlxPoint(350, 130), new FlxPoint(310, 130), new FlxPoint(310, 170),
				new FlxPoint(230, 170), new FlxPoint(230, 290), new FlxPoint(310, 290), new FlxPoint(315, 630)];
			path_2 = [
				new FlxPoint(670, 5), new FlxPoint(670, 50), new FlxPoint(630, 50), new FlxPoint(630, 130), new FlxPoint(670, 130), new FlxPoint(670, 170),
				new FlxPoint(750, 170), new FlxPoint(750, 290), new FlxPoint(670, 290), new FlxPoint(670, 630)];
			path_3 = [
				new FlxPoint(310, 5),
				new FlxPoint(310, 50),
				new FlxPoint(470, 50),
				new FlxPoint(470, 410),
				new FlxPoint(670, 410),
				new FlxPoint(670, 630)
			];
			nbSpawners = 3;
		}
		else
		{
			map = new FlxOgmo3Loader(AssetPaths.ludum_dare_04_2020__ogmo, AssetPaths.south__json);
			path_1 = [
				new FlxPoint(310, 5),
				new FlxPoint(310, 90),
				new FlxPoint(230, 90),
				new FlxPoint(230, 410),
				new FlxPoint(350, 410),
				new FlxPoint(470, 550),
				new FlxPoint(470, 630)
			];
			path_2 = [
				new FlxPoint(670, 5),
				new FlxPoint(670, 90),
				new FlxPoint(750, 90),
				new FlxPoint(750, 410),
				new FlxPoint(640, 410),
				new FlxPoint(510, 550),
				new FlxPoint(510, 630)
			];
			path_3 = [];
			nbSpawners = 2;
		}
		walls = map.loadTilemap(AssetPaths.overworld_tileset_grass__png, "Background");
		walls.setPosition(180, 0);
		walls.scale.set(2.5, 2.5);

		spawners = new Array<FlxPoint>();
		towerSpawners = new Array<TowerSpawner>();
		map.loadEntities(loadSpawns, "Entities");

		add(walls);
		add(playButton);
		add(text);
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);
		if (FlxG.mouse.pressed)
		{
			Console.log("Mouse: " + FlxG.mouse.x + "," + FlxG.mouse.y);
			var dist:Float = Math.POSITIVE_INFINITY;
			var p:TowerSpawner = towerSpawners[0];
			for (tSpawn in towerSpawners)
			{
				if (dist > FlxG.mouse.getPosition().distanceTo(tSpawn.getPosition()))
				{
					dist = FlxG.mouse.getPosition().distanceTo(tSpawn.getPosition());
					p = tSpawn;
				}
			}
			if (p.empty)
			{
				p.empty = false;
				var t:Tower = new Tower(p.getPosition().x, p.getPosition().y, normal);
				towers.push(t);
				add(t);
			}
		}

		if (FlxG.keys.justReleased.A)
		{
			wave = new Wave([REGULAR, REGULAR, REGULAR], [2, 3, 6]);
		}

		if (wave != null && !wave.finished)
			wave.update(this);
		else
			wave = null;

		// Tire sur les ennemis
		for (tower in towers)
		{
			for (enemy in enemies)
			{
				if (!enemy.isSoonDead())
				{
					var nShot:Shot = tower.fire(enemy);
					if (nShot != null)
					{
						shots.push(nShot);
						add(nShot);
					}
				}
			}
		}

		// Suppression
		var i = enemies.length;
		while (i-- > 0)
		{
			if (enemies[i].isDead())
			{
				remove(enemies[i]);
				enemies.splice(i, 1);
			}
		}

		i = shots.length;
		while (i-- > 0)
		{
			if (shots[i].isDead())
			{
				remove(shots[i]);
				shots.splice(i, 1);
			}
		}
	}

	public function loadSpawns(entity:EntityData):Void
	{
		var nX:Float = 180 + entity.x * 2.5;
		var nY:Float = entity.y * 2.5;

		if (entity.name == "spawner")
		{
			spawners.push(new FlxPoint(nX, nY));
		}
		else if (entity.name == "turrets_spawner")
		{
			towerSpawners.push(new TowerSpawner(nX, nY));
		}
	}

	public function spawnEnemy(type:EnemyType)
	{
		var r:Int = FlxG.random.int(0, nbSpawners - 1);
		var e:Enemy = null;
		if (r == 0)
			e = new Enemy(path_1[0].x, path_1[0].y, path_1, type);
		if (r == 1)
			e = new Enemy(path_2[0].x, path_2[0].y, path_2, type);
		if (r == 2)
			e = new Enemy(path_3[0].x, path_3[0].y, path_3, type);

		enemies.push(e);
		add(e);
	}
}
