package views;

import entities.Enemy;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.addons.editors.ogmo.FlxOgmo3Loader;
import flixel.group.FlxGroup;
import flixel.text.FlxText.FlxTextAlign;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import openfl.text.TextFieldAutoSize;

class Castle extends FlxGroup
{
	var playButton1:FlxButton;
	var playButton2:FlxButton;
	var playButton3:FlxButton;
	var playButton4:FlxButton;

	var map:FlxOgmo3Loader;
	var walls:FlxTilemap;

	var enemy:Enemy;
	var enemies:FlxTypedGroup<Enemy>;

	public function new()
	{
		super();
		enemies = new FlxTypedGroup<Enemy>();

		var text = new flixel.text.FlxText(10, 10, 0, "Castle", 24);
		add(text);
		playButton1 = new FlxButton(10, 50, "East", function()
		{
			Main.game.changeState(EAST);
		});
		playButton2 = new FlxButton(10, 100, "West", function()
		{
			Main.game.changeState(WEST);
		});
		playButton3 = new FlxButton(10, 150, "North", function()
		{
			Main.game.changeState(NORTH);
		});
		playButton4 = new FlxButton(10, 200, "South", function()
		{
			Main.game.changeState(SOUTH);
		});

		playButton1.label.setFormat(20, FlxColor.BLACK, FlxTextAlign.CENTER);
		playButton2.label.setFormat(20, FlxColor.BLACK, FlxTextAlign.CENTER);
		playButton3.label.setFormat(20, FlxColor.BLACK, FlxTextAlign.CENTER);
		playButton4.label.setFormat(20, FlxColor.BLACK, FlxTextAlign.CENTER);

		playButton1.scale.set(2, 2);
		playButton2.scale.set(2, 2);
		playButton3.scale.set(2, 2);
		playButton4.scale.set(2, 2);

		playButton1.updateHitbox();
		playButton2.updateHitbox();
		playButton3.updateHitbox();
		playButton4.updateHitbox();

		map = new FlxOgmo3Loader(AssetPaths.ludum_dare_04_2020__ogmo, AssetPaths.castle__json);
		walls = map.loadTilemap(AssetPaths.overworld_tileset_grass__png, "Background");
		walls.setPosition(180, 0);
		walls.scale.set(2.5, 2.5);
		add(walls);
		add(playButton1);
		add(playButton2);
		add(playButton3);
		add(playButton4);
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);
	}
}
