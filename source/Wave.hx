package;

import entities.Enemy;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.addons.display.FlxNestedSprite;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;
import views.Land;

class Wave
{
	var types:Array<EnemyType>;
	var numbers:Array<Int>;
	var time:FlxTimer;

	public var finished:Bool;

	public function new(t:Array<EnemyType>, n:Array<Int>)
	{
		types = t;
		numbers = n;
		finished = false;

		time = new FlxTimer();
		time.start();
	}

	public function update(land:Land)
	{
		if (time.finished)
		{
			if (types.length > 0)
			{
				var r:Int = FlxG.random.int(0, types.length);
				land.spawnEnemy(types[r]);
				numbers[r]--;
				if (numbers[r] == 0)
				{
					numbers.splice(r, 1);
					types.splice(r, 1);
				}

				time.start(FlxG.random.float(1.0, 2.0));
			}
			else
			{
				finished = true;
			}
		}
	}
}
