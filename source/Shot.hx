package;

import entities.Enemy;
import flixel.FlxSprite;
import flixel.addons.display.FlxNestedSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;
import js.html.Console;

class Shot extends FlxSprite
{
	var damages:Int;
	var goal:Enemy;
	var speed:Float;
	var dead:Bool;

	public function new(x:Float, y:Float, g:Enemy, d:Int)
	{
		super(x, y);
		goal = g;
		damages = d;
		speed = 200;
		dead = false;

		makeGraphic(10, 5, FlxColor.RED);
	}

	override function update(elapsed:Float)
	{
		super.update(elapsed);

		if (goal != null)
		{
			var newAngle:Float;

			if (x < goal.x)
			{
				if (y <= goal.y)
					newAngle = Math.atan(Math.abs(y - goal.y) / Math.abs(x - goal.x)) * 180 / Math.PI;
				else
					newAngle = -Math.atan(Math.abs(y - goal.y) / Math.abs(x - goal.x)) * 180 / Math.PI;
			}
			else
			{
				if (y <= goal.y)
					newAngle = (Math.PI - Math.atan(Math.abs(y - goal.y) / Math.abs(x - goal.x))) * 180 / Math.PI;
				else
					newAngle = (Math.PI + Math.atan(Math.abs(y - goal.y) / Math.abs(x - goal.x))) * 180 / Math.PI;
			}

			var dx:Float = goal.x - x;
			var dy:Float = goal.y - y;

			var max:Float = Math.max(Math.abs(dx), Math.abs(dy));

			velocity.set(speed * dx / max, speed * dy / max);
			angle = newAngle;

			if (goal.getPosition().distanceTo(getPosition()) < 1)
			{
				dead = true;
				goal.hit(damages);
			}
		}
		else
		{
			dead = true;
		}
	}

	public function isDead():Bool
	{
		return dead;
	}
}
