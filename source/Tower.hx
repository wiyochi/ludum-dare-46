package;

import flixel.system.FlxSound;
import entities.Enemy;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.addons.display.FlxNestedSprite;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;

enum TowerType
{
	normal;
	destroyer;
	aoe;
	defense;
}

class Tower extends FlxSprite
{
	var type:TowerType;
	var life:Int;
	var damages:Int;
	var fireRate:Float;
	var range:Float;
	var cost:Int;
	var level:Int;
	var levelBonus:Array<Float>;
	var time:FlxTimer;
	var shootSound:FlxSound;

	public function new(x:Float = 0, y:Float = 0, t:TowerType = normal)
	{
		super(x, y);
		time = new FlxTimer();
		time.start();

		loadGraphic(AssetPaths.Tour_1__png, false, 16, 16);
		shootSound = FlxG.sound.load(AssetPaths.shoot__wav);
		
		scale.set(0.13, 0.13);
		this.y -= 20;
		updateHitbox();
		// makeGraphic(40, 40, FlxColor.BLUE);

		type = t;
		switch (type)
		{
			case normal:
				life = 5;
				damages = 3;
				fireRate = 1;
				range = 120;
				cost = 5;
				levelBonus = [1, 1, 0.25];
			case destroyer:
				life = 8;
				damages = 6;
				fireRate = 1;
				range = 160;
				cost = 20;
				levelBonus = [1, 1, 0];
			case aoe:
				life = 3;
				damages = 8;
				fireRate = 0.25;
				range = Math.POSITIVE_INFINITY;
				cost = 9;
				levelBonus = [1, 1, 0];
			case defense:
				life = 10;
				damages = 1;
				fireRate = 0.5;
				range = 80;
				cost = 5;
				levelBonus = [2, 1, 0];
		}
	}

	public function fire(goal:Enemy):Shot
	{
		if (time.finished && getPosition().distanceTo(goal.getPosition()) < range)
		{
			time.start(fireRate);
			shootSound.play();
			return new Shot(getPosition().x, getPosition().y, goal, damages);
		}
		return null;
	}

	public function upgrade()
	{
		life += cast(levelBonus[0], Int);
		damages += cast(levelBonus[1], Int);
		fireRate += levelBonus[2];
	}

	public function hit()
	{
		life -= 1;
	}
}
