package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.addons.display.FlxNestedSprite;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;

class EP extends FlxSprite
{
	public function new(x:Float = 0, y:Float = 0)
	{
		super(x, y);

		makeGraphic(20, 20, FlxColor.GREEN);
	}

	override function update(elapsed:Float)
	{
		super.update(elapsed);

		setPosition(FlxG.mouse.x, FlxG.mouse.y);
	}
}
