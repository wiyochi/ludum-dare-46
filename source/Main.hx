package;

import flixel.FlxG;
import flixel.FlxGame;
import flixel.system.debug.log.LogStyle;
import openfl.display.Sprite;
import views.GameState;
import views.Land;

class Main extends Sprite
{
	public static var game:GameState = new GameState();

	public function new()
	{
		super();

		addChild(new FlxGame(1280, 720, PlayState, true));
		FlxG.mouse.useSystemCursor = true;
		FlxG.switchState(game);
	}
}
