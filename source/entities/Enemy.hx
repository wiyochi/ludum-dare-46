package entities;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.system.FlxSound;
import flixel.util.FlxPath;
import flixel.util.FlxTimer;

enum EnemyType
{
	REGULAR;
	BOSS;
}

class Enemy extends FlxSprite
{
	static inline var SPEED:Float = 50;

	var deadSound:FlxSound = FlxG.sound.load(AssetPaths.explosion__wav);

	var life:Int;
	var dead:Bool = false;
	var soonDead:Bool;
	var time:FlxTimer;

	public function new(x:Float, y:Float, p:Array<FlxPoint>, t:EnemyType)
	{
		super(x, y);
		var graphic = AssetPaths.Run_000__png;
		loadGraphic(graphic, false);
		setGraphicSize(32, 32);
		updateHitbox();
		soonDead = false;
		time = new FlxTimer();

		life = 4;
		dead = false;
		path = new FlxPath();
		for (ppoint in p)
		{
			path.add(ppoint.x, ppoint.y);
		}
		path.start(SPEED, FlxPath.FORWARD);
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);

		if (soonDead && time.finished)
		{
			dead = true;
		}
	}

	public function hit(damages:Int)
	{
		life -= damages;
		if (life <= 0)
		{
			deadSound.play();
			soonDead = true;
			loadGraphic(AssetPaths.TradingIcons_32_t__png, false);
			setGraphicSize(32, 32);
			updateHitbox();
			path.cancel();

			time.start(0.5);
		}
	}

	public function isDead()
	{
		return dead;
	}

	public function isSoonDead()
	{
		return soonDead;
	}
}
